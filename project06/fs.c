#include "fs.h"
#include "disk.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <math.h>
  
  
  
  
/************************************************************/
#define TAKEN 1
#define FREE 0
#define FS_MAGIC           0xf0f03410
#define INODES_PER_BLOCK   128
#define POINTERS_PER_INODE 5
#define POINTERS_PER_BLOCK 1024
#define BLOCK_SIZE 4096
#define MIN(a,b) (((a)<(b))?(a):(b))

int *bitmap = NULL;

/************************************************************/

typedef struct fs_superblock superblock;
typedef union fs_block block;
typedef struct fs_inode inode;

void print_valid_inode(inode inode, int i, int j);
void print_superblock(block block);
int is_inumbet_invalid(int inumber);
int is_mounted();
int is_formated();
int num_of_blocks_in_node(inode node);
void init_inode(block tmp_block,int block_num, int node_num);
int do_mount();
int hendel_with_mounte();
inode *get_inode(int inumber);
void is_magic();
int find_block_free();


/*superblock *get_superblock();*/

/************************************************************/


struct fs_superblock {
	int magic;
	int nblocks;
	int ninodeblocks;
	int ninodes;
};

struct fs_inode {
	int isvalid;
	int size;
	int direct[POINTERS_PER_INODE];
	int indirect; // This is the number of bytes to be copied.
};

union fs_block {
	struct fs_superblock super;
	struct fs_inode inode[INODES_PER_BLOCK];
	int pointers[POINTERS_PER_BLOCK];
	char data[DISK_BLOCK_SIZE];
};





/************************fs_format*********************************/


int fs_format()
{
	if (is_mounted())
	{
		printf("the disk already mounted\n");
		return 0;
	}

	// create the SB members
	int nblocks = disk_size();
	int ninodeblocks = (int)(nblocks * 0.1);

	if (nblocks % 10 > 0)
	{
		ninodeblocks++;
	}
	
	int ninodes = ninodeblocks * INODES_PER_BLOCK;

	//init SB struct
	block bloc;

	bloc.super.magic = FS_MAGIC;
	bloc.super.nblocks = nblocks;
	bloc.super.ninodeblocks = ninodeblocks;
	bloc.super.ninodes = ninodes;
	// write SB to first 4KB
	disk_write(0, bloc.data);

	//travers all the inodes and init to 0
	for (int i = 1; i < ninodeblocks; i++)
	{
		block inode_block;

		for (int j = 0; j < INODES_PER_BLOCK; j++)
		{
			inode_block.inode[j].isvalid = 0;
			inode_block.inode[j].size = 0;
			inode_block.inode[j].indirect = 0;

			
			for (int i = 0; i < POINTERS_PER_INODE; i++)
			{
				(inode_block.inode[j].direct)[i] = 0;
			}
		}
		
		disk_write(i, inode_block.data);
	}
	
	return 1;


}
/************************fs_debug************************************/

void fs_debug()
{
	union fs_block block;
	disk_read(0,block.data);

	print_superblock(block);


	int ninodeblocks = block.super.ninodeblocks;
	if (ninodeblocks < 0){return;}
	for (int i = 0; i< ninodeblocks; i++){ // each inode block
		disk_read(i+1,block.data);
		for (int j = 0; j < INODES_PER_BLOCK; j++){ // each inode
			struct fs_inode inode = block.inode[j];
			
			print_valid_inode(inode, i, j);
		}
	}
}

/************************fs_mount************************************/


//build a new free block bitmap
int fs_mount()
{
	if (is_mounted())
	{
		return 0;
	}

	block superblock;
	disk_read(0,superblock.data);

	int nblocks = superblock.super.nblocks;
	int ninodeblocks = superblock.super.ninodeblocks;

	struct fs_inode inode;
	union fs_block datablock;

	bitmap = (int *) malloc(nblocks);
	bitmap[0] = 1;

	for(int i = 1; i <= ninodeblocks; i++)
	{
		bitmap[i] = 1;
		disk_read(i,datablock.data);

		for(int j = 0; j < INODES_PER_BLOCK; j++)
		{
			inode = datablock.inode[j];

			if(inode.isvalid){
				int fileblocks = inode.size / BLOCK_SIZE;

				if(inode.size % BLOCK_SIZE)
				{
					fileblocks++;
				}

				for(int k = 0; k < POINTERS_PER_INODE; k++)
				{
					bitmap[inode.direct[k]] = 1;
				}

				if(fileblocks > POINTERS_PER_INODE){
					disk_read(inode.indirect,datablock.data);

					for(int k = 0; k < (fileblocks - POINTERS_PER_INODE); k++){
						bitmap[datablock.pointers[k]] = 1;
					}
				}
			}
		}
	}

	return 1;

}

/************************fs_create************************************/


int fs_create()
{
	if (!hendel_with_mounte())
	{
		return 0;
	}
	block superblock;
	disk_read(0,superblock.data);
	
	for(int i = 1; i <= superblock.super.ninodeblocks; i++)
	{
		block block2;
		disk_read(i, block2.data);

		for(int j = 0; j < INODES_PER_BLOCK; j++)
		{
			if(!(block2.inode[j].isvalid))
			{
				block2.inode[j].isvalid = 1;
				block2.inode[j].size = 0;
				disk_write(i, block2.data);
				printf("create with an inumber of : %d\t", (i - 1) * INODES_PER_BLOCK + j + 1);
				return (i - 1) * INODES_PER_BLOCK + j + 1;
			}
		}
	}
	return -1;
}
/*********************************fs_delete***************************************/

int fs_delete( int inumber )
{
	if (is_inumbet_invalid(inumber) || !hendel_with_mounte())
	{
		return 0;
	}

	int block_num = 1 + ((inumber - 1) / INODES_PER_BLOCK);
	int node_num = (inumber - 1) % INODES_PER_BLOCK;

	block tmp_block;
	disk_read(block_num, tmp_block.data);
	inode my_node = (tmp_block.inode)[node_num];

	if(my_node.isvalid)
	{
		int num_of_bloks = num_of_blocks_in_node(my_node);

		for (int i = 0; i < POINTERS_PER_INODE; i++)
		{
			int i_direct = my_node.direct[i];

			if(i_direct)
			{
				bitmap[i_direct] = 0;
				/*(tmp_block.inode[node_num].direct)[i] = 0;*/
			}
		}

		if(num_of_bloks > POINTERS_PER_INODE)
		{
			block indirect_block;
			disk_read(my_node.indirect, indirect_block.data);
			
			int num_blocks_of_indirect = num_of_bloks - POINTERS_PER_INODE;

			for (int i = 0; i < num_blocks_of_indirect; i++)
			{
				if((indirect_block.pointers)[i])
				{
					bitmap[(indirect_block.pointers)[i]] = 0;
					/*(indirect_block.pointers)[i] = 0;*/
				}
				
			}
		}	
		init_inode(tmp_block, block_num, node_num);
	
	}

	return 1;
}

/*********************************fs_getsize***************************************/



int fs_getsize( int inumber )
{
	if (!hendel_with_mounte())
	{
		return 0;
	}
	inode* my_node = get_inode(inumber);
	int size = my_node->size;
	free(my_node);
	return size;
}

/*********************************fs_read***************************************/

int fs_read( int inumber, char *data, int length, int offset )
{	
	if (is_inumbet_invalid(inumber) || !hendel_with_mounte())
	{
		return 0;
	}
	int blocknum = (inumber - 1) /INODES_PER_BLOCK + 1;
	int inodenum = (inumber - 1) %INODES_PER_BLOCK;
	
	is_magic();
	union fs_block block;
	disk_read(blocknum, block.data);
	struct fs_inode inode = block.inode[inodenum];
	if(!inode.isvalid || inode.size < offset)
	{
		return 0;
	}

	int copysize = MIN(inode.size - offset, length);

	if(copysize > 0)
	{
		union fs_block indirect;
		disk_read(inode.indirect, indirect.data);
		// a
		int firs_block = offset / BLOCK_SIZE;
		int block_offset = offset % BLOCK_SIZE;
		int first_length = (copysize > (BLOCK_SIZE - block_offset)) ? BLOCK_SIZE - block_offset : copysize;
		//b
		int num_of_fullblock = (copysize > (BLOCK_SIZE - offset)) ? (copysize - (BLOCK_SIZE - block_offset)) / BLOCK_SIZE : 0;
		//c	
		int last_block = firs_block + num_of_fullblock + 1;
		int last_length = (copysize > first_length)?(copysize - first_length) % BLOCK_SIZE : 0;
		

		//a
		union fs_block datablock;
		
		if(firs_block < POINTERS_PER_INODE)
		{
			disk_read(inode.direct[firs_block], datablock.data);
		}
		else
		{
			int tempblocknum = indirect.pointers[firs_block - POINTERS_PER_INODE];
			disk_read(tempblocknum, datablock.data);
		}

		memcpy(data, datablock.data + block_offset, first_length); //why is not take a diffrent form offset ? "datablock.data + blockoffset"
	
		//b
		for(int i = 1; i <= num_of_fullblock; i++){
			
			union fs_block tempblock;

			if (firs_block + i < POINTERS_PER_INODE)
			{
				disk_read(inode.direct[firs_block + i], tempblock.data);
			}
			else
			{
				int tempblocknum = indirect.pointers[firs_block + i - POINTERS_PER_INODE];
				disk_read(tempblocknum, tempblock.data);
			}

			memcpy(data + first_length + BLOCK_SIZE * (i-1), tempblock.data, BLOCK_SIZE);
		}

		//c
		if(last_length)
		{
			
			union fs_block tempblock;
			if (last_block < POINTERS_PER_INODE)
			{
				disk_read(inode.direct[last_block], tempblock.data);
			}
			else
			{
				int tempblocknum = indirect.pointers[last_block - POINTERS_PER_INODE];
				disk_read(tempblocknum, tempblock.data);
			}

			memcpy(data + first_length + BLOCK_SIZE * num_of_fullblock, tempblock.data, last_length);
			
		}
	}
	return copysize;

}

/*********************************fs_write***************************************/


int fs_write( int inumber, const char *data, int length, int offset )
{
	if (is_inumbet_invalid(inumber) || !hendel_with_mounte())
	{
		return 0;
	}
	int blocknum = (inumber - 1) /INODES_PER_BLOCK + 1;
	int inodenum = (inumber - 1) %INODES_PER_BLOCK;
	
	is_magic();
	union fs_block block;
	disk_read(blocknum, block.data);
	struct fs_inode inode = block.inode[inodenum];
	if(!inode.isvalid || inode.size < offset)
	{
		return 0;
	}
	int ret = 0;
	int blockbegin = offset / BLOCK_SIZE;
	int blockoffset = offset % BLOCK_SIZE;
	int first_length = (length > BLOCK_SIZE - blockoffset) ? BLOCK_SIZE - blockoffset : length;	

	int datablocknum = (length > (BLOCK_SIZE - offset))?(length - (BLOCK_SIZE - blockoffset)) / BLOCK_SIZE : 0;
	
	int last = (first_length < length)?(length - first_length) % BLOCK_SIZE : 0;

	int directblocknum = datablocknum + blockbegin + 1;

	for(int i = 0; i < directblocknum; i++){
		if(inode.direct[i] != 0)
			continue;
		int freeblock = find_block_free();
		if(freeblock != -1)
		{
			bitmap[freeblock] = TAKEN;
			inode.direct[i] = freeblock;
		}
	}
	
	int extrablock = datablocknum + blockbegin + 1 - POINTERS_PER_INODE;
	// allocate indirect pointer
	if(extrablock > 0 && inode.indirect == 0){
		int freeblock = find_block_free();
		if(freeblock != -1){
			bitmap[freeblock] = TAKEN;
			inode.indirect = freeblock;
		}
	}
	if(extrablock > 0){
		union fs_block indirect;
		disk_read(inode.indirect, indirect.data);
		// allocate space for data
		for(int k = 0; k < extrablock; k++){
			if(indirect.pointers[k] != 0)
				continue;
			int tempfree = find_block_free();
			if(tempfree != -1){
				bitmap[tempfree] = TAKEN;
				indirect.pointers[k] = tempfree;
			}else{
				indirect.pointers[k] = 0;
			}
		}
		disk_write(inode.indirect, indirect.data);
	}
	
	block.inode[inodenum] = inode;
	disk_write(blocknum, block.data);

	//a
	union fs_block indirect;
	disk_read(inode.indirect, indirect.data);
	union fs_block datablock;
	int tempblocknum;
	if(blockbegin > POINTERS_PER_INODE){
		tempblocknum = indirect.pointers[blockbegin - POINTERS_PER_INODE];
	}else{
		tempblocknum = inode.direct[blockbegin];
	}

	if(tempblocknum == 0){
		block.inode[inodenum].size += ret;
		disk_write(blocknum, block.data);
		return ret;
	}
	
	disk_read(tempblocknum, datablock.data);
	memcpy(datablock.data + blockoffset, data, first_length);
	disk_write(inode.direct[blockbegin], datablock.data);
	ret += first_length;

	//b
	for(int i = 1; i <= datablocknum; i++){
		if(blockbegin + i < POINTERS_PER_INODE){
			if(inode.direct[blockbegin + i] == 0){
				block.inode[inodenum].size += ret;
				disk_write(blocknum, block.data);
				return ret;
			}
			disk_write(inode.direct[blockbegin + i], data+first_length + BLOCK_SIZE*(i-1));
		}else{
			int tempblocknum = indirect.pointers[blockbegin + i - POINTERS_PER_INODE];
			if(tempblocknum == 0){
				block.inode[inodenum].size += ret;
				disk_write(blocknum, block.data);
				return ret;
			}
			disk_write(tempblocknum, data + first_length + BLOCK_SIZE*(i-1));
		}
		ret += BLOCK_SIZE;
	}
	//c
	if(last != 0)
	{
		union fs_block tempblock;
		
		if(blockbegin + datablocknum < POINTERS_PER_INODE){
			if(inode.direct[blockbegin + datablocknum] == 0){
				block.inode[inodenum].size += ret;
				disk_write(blocknum, block.data);
				return ret;
			}
			disk_read(inode.direct[blockbegin+datablocknum], tempblock.data);
			memcpy(tempblock.data, data + first_length + BLOCK_SIZE * datablocknum, last);
			disk_write(inode.direct[blockbegin + datablocknum], tempblock.data);
		}else{
			int tempblocknum = indirect.pointers[blockbegin + datablocknum - POINTERS_PER_INODE];
			if(tempblocknum == 0){
				block.inode[inodenum].size += ret;
				disk_write(blocknum, block.data);
				return ret;
			}
			memcpy(tempblock.data, data + first_length + BLOCK_SIZE * datablocknum, last);
			disk_write(tempblocknum, tempblock.data);
		}
		ret += last;
		
	}

	block.inode[inodenum].size += ret;
	disk_write(blocknum, block.data);
	return ret;

}
/***************************************************************************/

int is_inumbet_invalid(int inumber)
{
	block new_block;
	disk_read(0, new_block.data);

	int num_of_inods = new_block.super.ninodes;

	if(inumber == 0 || inumber > num_of_inods)
	{
		printf("error, your inumber is invalid");
		return 1;
	}

	return 0;
}

int is_mounted()
{
	if(bitmap != NULL)
	{
		return 1;
	}

	return 0;
}

int num_of_blocks_in_node(inode node)
{
	int blocks = node.size / BLOCK_SIZE;
	 if (node.size % BLOCK_SIZE)
	 {
		 blocks++;
	 }

	 return blocks;
}

void init_inode(block tmp_block,int block_num, int node_num)
{
	tmp_block.inode[node_num].isvalid = 0;
	tmp_block.inode[node_num].size = 0;
	memset(tmp_block.inode[node_num].direct, 0, POINTERS_PER_INODE * 4);
	tmp_block.inode[node_num].indirect = 0;

	disk_write(block_num, tmp_block.data);
}

void print_superblock(block block)
{
	printf("superblock:\n");

	if (block.super.magic == FS_MAGIC){
		printf("\tmagic number is valid\n");
	}
	else {
		printf("\tmagic number is not valid\n");
	}
	printf("\t%d blocks on disk\n",block.super.nblocks);
	printf("\t%d blocks for inodes\n",block.super.ninodeblocks);
	printf("\t%d inodes total\n",block.super.ninodes);

}
void print_valid_inode(inode inode, int i, int j)
{
	if (inode.isvalid)
	{	
		printf("inode %d:\n", j+i*INODES_PER_BLOCK + 1);
		printf("\tsize: %d bytes\n",fs_getsize(j+i*INODES_PER_BLOCK + 1));
		printf("\tdirect blocks: ");
		for (int k = 0;k<POINTERS_PER_INODE; k++){
			int pointedblock = inode.direct[k];
			if (pointedblock != 0){
				printf("%d ", pointedblock);
			}
		}
		printf("\n");

		if (inode.indirect)
		{
			printf("    indirect block: %d\n", inode.indirect);
			printf("    indirect data blocks: "); 

			block indirectblock;
			disk_read(inode.indirect, indirectblock.data);
			for (int l = 0; l < POINTERS_PER_BLOCK; l++){
							if (indirectblock.pointers[l]!=0){
					printf("%d ",indirectblock.pointers[l]);
				}
			
			}
			printf("\n");
		}
	}

}

int do_mount()
{
	char answer;

	printf("your disk is unmount\ndo you wont to mounted ?( y/n)\n");
	answer = getchar();

	if (answer == 'y')
	{
		fs_mount();
		return 1;
	}

	return 0;
}

int hendel_with_mounte()
{
	if (!is_mounted() && !do_mount())
	{
		return 0;
	}
	return 1;
}

inode *get_inode(int inumber)
{
	union fs_block superblock;
	disk_read(0, superblock.data);	
	
	if(inumber > superblock.super.ninodes || inumber == 0)
	{
		return NULL;
	}

	int blocknum = (inumber - 1) /INODES_PER_BLOCK + 1;
	int inodenum = (inumber - 1) %INODES_PER_BLOCK;

	union fs_block block;
	disk_read(blocknum, block.data);

	inode *my_node = (inode*)malloc(sizeof(inode));
	*my_node = block.inode[inodenum];

	if(my_node->isvalid == 0){
		return NULL;
	}	
	return my_node;
}

int find_block_free(){
	if(!is_mounted()){
		printf("The disk haven't been mounted!\n");
		return -1;
	}
	union fs_block block;
	disk_read(0, block.data);
	int nblocks = block.super.nblocks;
	int inodesblocks = block.super.ninodeblocks;
	for(int i = inodesblocks; i < nblocks; i++){
		if(bitmap[i] == FREE){
			return i;
		}
	}
	return -1;
}

superblock *get_superblock()
{
	block temp_superblock;
	disk_read(0,temp_superblock.data);

	superblock *my_superblock = (superblock*)malloc(sizeof(temp_superblock.data));
	return my_superblock;	
}
void is_magic()
{
	union fs_block block;
	disk_read(0,block.data);

	if(block.super.magic != FS_MAGIC)
	{
		exit(0);
	}
}



/************************************************************/
/*



void for_each(void (*fun)(void *))
{
    union fs_block block;

    disk_read(0,block.data);


    int ninodeblocks = block.super.ninodeblocks;
    int ninodeblocks = block.super.ninodeblocks;
    if (ninodeblocks < 0){return;}
    for (int i = 0; i< ninodeblocks; i++){ // each inode block
        disk_read(i+1,block.data);
        for (int j = 0; j < INODES_PER_BLOCK; j++){ // each inode
            struct fs_inode inode = block.inode[j];
            
            (*fun)(inode, i, j);
        }
  }
}
*/
