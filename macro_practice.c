#include <stdio.h>

#define AREA_OF_CUBE(a) (6 * ((a) * (a)))
#define PI 3.1415926
#define AREA_OF_CYLINDER(r, h) (((2/3)*(PI)) * (((r)*(r))*(h)))


int main()
{
    int edge = 5, r = 4, h = 5;
    double res = AREA_OF_CYLINDER(r, h);
    
    printf("%d\n",AREA_OF_CUBE(edge));
    printf("%lu\n",res);
}
