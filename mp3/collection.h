/*
Why can templates only be implemented in the header file?

https://stackoverflow.com/questions/495021/why-can-templates-only-be-implemented-in-the-header-file
*/

#include <iostream>
#include <string>
#include <vector>


using namespace std;

/*----------------------------Song---------------------------------------*/
class Song
{
private:
    
public:
    string song;
    string artist;
    string lyrics;
    string folder;


    Song(string song_name, string artist_name, 
        string lyrics_song,string master_folder) {
        song = song_name;
        artist = artist_name;
        lyrics = lyrics_song;
        folder = master_folder;
        }
    Song(const Song&); // copy constructor
    ~Song(){};

    void operator = (const Song &obj) {
        song = obj.song;
        artist = obj.artist;
        lyrics = obj.lyrics;
        folder = obj.folder;
    }
    bool operator > (const Song &obj) {
        return song > obj.song;
    }

};

/*----------------------------Iterator---------------------------------------*/
template <typename T, typename U>
class Iterator {
 public:
  typedef typename vector<T>::iterator iter_type;
  Iterator(U *ptr, bool reverse = false) {
    my_collection = ptr;
    my_iterator = my_collection->my_vector.begin();
  }

  iter_type Next() {
    if(HasNext()) {
      return ++my_iterator;
    }
    else {
      
      return my_iterator;
    }
    
  }

  bool HasNext() {
    return (my_iterator != my_collection->my_vector.end());
  }

  Song *Current() {
    return (*my_iterator);
  }

  iter_type Remove() {
      
      my_iterator = my_collection->my_vector.erase(my_iterator);
      return my_iterator;

  }

 private:
  
  U *my_collection;
  iter_type my_iterator;
};

/*-------------------------------Collection------------------------------------*/

template <class T>
class Collection {
  friend class Iterator<T, Collection>;

 public:
  void Add(T a) {
    
    int offset = 0;
    auto it = my_vector.begin();
    int size = my_vector.size();
    for (int i = 0; i < size; i++)
    {
      if ((a->song) <= ((*(it+i))->song)) {
        break;
        }
        else {
          offset++;
        }
    }
    my_vector.emplace(my_vector.begin() + offset, a);
    
   
    
  }
  int Size(){
    return my_vector.size();
  }
  Iterator<T, Collection> *GetIterator() {
    return new Iterator<T, Collection>(this);
  }

 private:
  vector<T> my_vector;
};



