#include <iostream>
#include "wav.h"

int main()
{
    MySongs mp3;

    cout << "-----------------add_folder test----------------\n" <<endl;
    mp3.add_folder("david");
    mp3.add_folder("shlomi");
    


    cout << "-----------------add_song test----------------\n" <<endl;

    mp3.add_song("All you need is love", "the Beatles", " Love, love, love Love, love, love.....","david");
    mp3.add_song("Come Together", "the Beatles", " Here come old flat top He come grooving up slowly.....","root");

    mp3.add_song("Break free", "Queen", "I want to break free I want to break free I want to ...","david");
    mp3.add_song("Hey you", "Pink floyd", "Hey you, out there in the cold Getting lonely....","root");
    mp3.add_song("Imagine", "John Lennon", "Imagine there's no heaven It's easy if you try ...","shlomi");
    mp3.add_song("High_Way_To_Hell", "AC_DC", " ...","shlomi");
    mp3.add_song("Rock_And_Roll", "LED ZEPPELIN", " ...","shlomi");


    cout << "-----------------print_folder_song test----------------" <<endl;
    mp3.print_folder_song("david", "Queen");

    cout << "-----------------play test----------------" <<endl;

    mp3.play("Rock_And_Roll", "shlomi");
    
/*
    cout << "-----------------print_songs test----------------" <<endl;

    mp3.print_songs();
    cout << "---------------\n";
    mp3.print_songs("the Beatles");

    cout << "-----------------move_song test----------------" <<endl;
    mp3.print_folder_song("david", "Queen");
    mp3.move_song("Break free", "shlomi", "david");
    mp3.print_folder_song("david", "Queen");
    mp3.print_folder_song("shlomi", "Queen");

    cout << "-----------------remove_folder test----------------" <<endl;
    
    mp3.remove_folder("david");
    mp3.play("Imagine", "david");

    cout << "-----------------remove_song test----------------" <<endl;
    mp3.play("Imagine","shlomi");
    mp3.remove_song("Imagine","shlomi");
    mp3.play("Imagine","shlomi");
    */
    return 0;
}