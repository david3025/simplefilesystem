#include <iostream>
#include <typeinfo>
using namespace std;


/*
float *func1(float n) {
    float *a = &n;
    (*a) = n + 5;
     do{
         (*a)--;
     }while (a > 0);
     
    return a;
}
*/
float *func1(float &n) {
    float *a = &n;
    (*a) = n + 5;
     do{
         (*a)--;
     }while (*a > 0);
     
    return a;
}

/*
char *IntToString(int number) {
    char str1[32] = {0};
    while (number != 0) {
        str1[i++] = '0' +number % 10;
        number /= 10;
    }
    return str1;
}
*/

void reverse(char str[], int length)
{
    int start = 0;
    int end = length -1;
    while (start < end)
    {
        swap(*(str+start), *(str+end));
        start++;
        end--;
    }
}

void IntToString(int number, char *string_input) {
    int i = 0;
    while (number != 0) {
        string_input[i++] = '0' + number % 10;
        number /= 10;
    }
    string_input[i] = '\0';
    reverse(string_input, i);
}

int main() {
    /*float num = -6;
    std::cout << *func1(num);*/
    char result[30];
    int number = 12345;
    IntToString(number, result);
    cout << typeid(number).name() << endl; 
    cout << typeid(result).name() << endl; 
    return 0;
}